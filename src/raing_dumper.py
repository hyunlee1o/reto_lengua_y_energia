from multiprocessing import Pool
from urllib.request import Request, urlopen
from lxml import etree

UA = "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0"
tid = 42
url_raiz = f"http://diccionario.raing.es/es/lemas?title=&title_op=contains&tid={tid}"
htmlparser = etree.HTMLParser()

def descargar_palabras(pagina):
    url = f"{url_raiz}&page={pagina}"
    req = Request(url, headers={"User-Agent": UA})
    with urlopen(req) as pagina_web:
        tree = etree.parse(pagina_web, htmlparser)
        elements = tree.xpath('//*[@class="view-content"]/div/ul/*/span/span/a')
    return [elem.text for elem in elements]


def escribir_palabras_archivo(palabras, tema=str(tid)):
    with open(f"dump_files/raing_{tema}.txt", "a") as fp:
        for palabra in palabras:
            fp.write(palabra + "\n")
            print(f"escribiendo => {palabra}")


def main():
    for pagina in range(32):
        palabras = descargar_palabras(pagina)
        escribir_palabras_archivo(palabras)


if __name__ == "__main__":
    main()
