import argparse

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('filename')
    args = parser.parse_args()
    pals = list(open(args.filename))
    print(args.filename)
    pals_norepe = list(set(pals))
    palabras_limpias = list(filter(lambda x: '-' not in x, pals_norepe))
    palabras_limpias.sort()
    with open(f"{args.filename.removesuffix('.txt')}_clean.txt", 'w') as f:
        f.writelines(pal for pal in palabras_limpias)
    
   
if __name__ == "__main__":
    main()
