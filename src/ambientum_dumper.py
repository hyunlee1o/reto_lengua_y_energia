import string
from urllib.request import Request, urlopen
from lxml import etree

UA = "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0"
url_raiz = f"https://www.ambientum.com/diccionario-de-terminos-medioambientales-letra/"
htmlparser = etree.HTMLParser()


def descargar_palabras(letra):
    url = f"{url_raiz}{letra}"
    req = Request(url, headers={"User-Agent": UA})
    # try:
    with urlopen(req) as webpage:
        tree = etree.parse(webpage, htmlparser)
        elements = tree.xpath('//*[@class="td-page-content"]/p/strong')
    return [elem.text for elem in elements]


def escribir_palabras_archivo(palabras):
    with open(f"dump_files/ambientum.txt", "a") as fp:
        for palabra in palabras:
            fp.write(palabra + "\n")
            print(f"escribiendo => {palabra}")


def main():
    for letra in list(string.ascii_lowercase):
        try:
            palabras = descargar_palabras(letra)
        except Exception:
            palabras = descargar_palabras(f"letra-{letra}")
            if not palabras:
                continue
        escribir_palabras_archivo(palabras)


if __name__ == "__main__":
    main()
