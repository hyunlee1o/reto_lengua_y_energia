import string
import re
from urllib.request import Request, urlopen
from lxml import etree

UA = "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0"
htmlparser = etree.HTMLParser()

regex = re.compile(r'[\n\r\t]')
def limpiar_palabras(words):
    try:
        cleaned_words = [' '.join(word.replace("\r\n\t\t\t\xa0", " ").split(" ")[1:]) for word in words]
    except Exception as e:
        return []
    return cleaned_words


def obtener_n_paginas(url):
    req = Request(url, headers={"User-Agent": UA})
    with urlopen(req) as pagina_web:
        tree = etree.parse(pagina_web, htmlparser)
        element = tree.xpath('//*[@id="frmresultados"]/p[@class="num_resultado"]')[1].text
        pages = int(regex.sub("", element).split(' ')[1])
    return pages


def descargar_palabras(url):
    req = Request(url, headers={"User-Agent": UA})
    # try:
    with urlopen(req) as pagina_web:
        tree = etree.parse(pagina_web, htmlparser)
        elements = tree.xpath('//*[@id="frmresultados"]/div[@class="resultado"]/p/a')
    return [elem.text for elem in elements]


def escribir_palabras_a_archivo(palabras, año):
    with open(f"dump_files/cvc_neologismos_{año}.txt", "a") as fp:
        for pal in palabras:
            fp.write(pal + "\n")
            print(f"escribiendo => {pal}")

    
def main():
    for año in range(2010, 2020):
        page_total = obtener_n_paginas(f"https://cvc.cervantes.es/lengua/banco_neologismos/resultados.asp?idioma=Espa%F1ol&anno={año}&pagina=1&txtbusqueda=&CG=&Tipus=&nombre=&Asp_tip=&Font=")
        for pagina in range(1, page_total+1):
            url = f"https://cvc.cervantes.es/lengua/banco_neologismos/resultados.asp?idioma=Espa%F1ol&anno={año}&pagina={pagina}&txtbusqueda=&CG=&Tipus=&nombre=&Asp_tip=&Font="
            try:
                pals = descargar_palabras(url)
            except Exception:
                continue
            palabras_limpias = limpiar_palabras(pals)
            escribir_palabras_a_archivo(palabras_limpias, año)
            print(f"Escrito {año=} {pagina=}")


if __name__ == "__main__":
    main()

