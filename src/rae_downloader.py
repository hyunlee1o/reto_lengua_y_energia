#!/usr/bin/env python3

# Desarrollado por Jorge Sanchez Serrano

import pickle
import time
import signal
import string
from multiprocessing import Pool
import os
from urllib.parse import quote
from urllib.request import Request, urlopen
import requests
from lxml import etree

UA = "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0"
url_raiz = "https://dle.rae.es/{}?m=31"
texto_quitar = "Ir a la entrada "
long_tex_quitar = len(texto_quitar)

alfabeto = list(string.ascii_lowercase) + ["á", "é", "í", "ó", "ú", "ü"]
letras = alfabeto

def descargar_palabras(inicio_palabras):
    url = url_raiz.format(quote(inicio_palabras))
    req = Request(url, headers={"User-Agent": UA})
    try:
        pagina_web = urlopen(req)
    except Exception:
        res = requests.get(url)
        if res.status_code == 429:
            waiting_time = int(res.headers.get("Retry-After"))
            time.sleep(waiting_time)
        pagina_web = urlopen(req)
    htmlparser = etree.HTMLParser()
    tree = etree.parse(pagina_web, htmlparser)
    ret = tree.xpath('//*[@id="resultados"]/*/div[@class="n1"]/a/@title')
    return ret


def guardar_lista_archivo(lista, letra):
    with open(f"dump_files/dump_palabras_{letra}.ob", "wb") as fp:
        pickle.dump(lista, fp)


def cargar_lista_archivo(letra):
    with open(f"dump_files/dump_palabras_{letra}.ob", "rb") as fp:
        return pickle.load(fp)


def write_words_to_file(res, letra):
    with open(f"dump_files/palabras_{letra}.txt", "a") as fp:
        for parsed_words in res:
            for word in parsed_words[long_tex_quitar:].split(", "):
                fp.write(word + "\n")


# ftodas = open("palabras_todas.txt", "w")
def dump_letra(letra):
    try:
        comienzos_palabras = cargar_lista_archivo(letra)
    except FileNotFoundError:
        comienzos_palabras = [letra]

    while comienzos_palabras:
        inicio_palabras = comienzos_palabras.pop(0)
        if inicio_palabras in ["js", "app", "docs"]:
            continue
        res = None
        print(f"{inicio_palabras=} Iniciando conexión")
        try:
            res = descargar_palabras(inicio_palabras)
        except Exception as e:
            print(e)
        finally:
            if res is None:
                comienzos_palabras = [inicio_palabras] + comienzos_palabras
                time.sleep(1)
                continue
        s = signal.signal(signal.SIGINT, signal.SIG_IGN)
        write_words_to_file(res, letra)
        if len(res) > 30:
            comienzos_palabras = list(
                set(comienzos_palabras + [inicio_palabras + letra for letra in alfabeto])
            )
            guardar_lista_archivo(comienzos_palabras, letra)
        time.sleep(5)
        signal.signal(signal.SIGINT, s)
    if not comienzos_palabras:
        os.remove(f"dump_files/dump_palabras_{letra}.ob")


if __name__ == "__main__":
    with Pool(len(letras)) as p:
        r = p.map(dump_letra, [letra for letra in letras])
    print("fin")
